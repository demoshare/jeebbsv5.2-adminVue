/*用户api接口*/
import fetch from './api'
import qs from 'qs'
/*会员组列表获取*/
export function getGroupList(params){
    return fetch({
       url:'/api/admin/group/list',
       method:'post',
       signValidate:false,
       data:params
    })
  }
/*会员组列表删除*/
export function deleteGroupList(params){
  return fetch({
     url:'/api/admin/group/delete',
     method:'post',
     signValidate:true,
     data:params
  })
}
/*上传接口*/
export function upload(params){
  return fetch({
     url:'/api/admin/upload/o_upload',
     method:'post',
     signValidate:false,
     data:params
  })
}
/*会员组字段获取接口*/
export function getGroup(params){
  return fetch({
     url:'/api/admin/group/get',
     method:'post',
     signValidate:false,
     data:params
  })
}
/*会员组字段修改接口*/
export function updateGroup(params){
  return fetch({
     url:'/api/admin/group/update',
     method:'post',
     signValidate:true,
     data:params
  })
}
/*会员组添加接口*/
export function addGroup(params){
  return fetch({
     url:'/api/admin/group/save',
     method:'post',
     signValidate:true,
     data:params
  })
}

/****
 *
 *
 *
 用户接口
 * **
 * **
 * *
 * */

/** 用户列表接口***/
export function getUserList(params){
  return fetch({
     url:'/api/admin/user/list',
     method:'post',
     signValidate:false,
     data:params
  })
}

/*官网团队用户列表接口 */
export function getOfficialList(params){
  return fetch({
     url:'/api/admin/user/official_list',
     method:'post',
     signValidate:false,
     data:params
  })
}


/** 获取用户或管理员 字段接口***/
export function getUserInfo(params){
  return fetch({
     url:'/api/admin/user/get',
     method:'post',
     signValidate:false,
     data:params
  })
}
/**用户修改**/
export function updateUserInfo(params){
  return fetch({
     url:'/api/admin/user/update',
     method:'post',
     signValidate:true,
     data:params
  })
}
/**用户添加**/
export function addUserInfo(params){
  return fetch({
     url:'/api/admin/user/save',
     method:'post',
     signValidate:true,
     data:params
  })
}
/**用户或管理员删除**/
export function deleteUserInfo(params){
  return fetch({
     url:'/api/admin/user/delete',
     method:'post',
     signValidate:true,
     data:params
  })
}
/*
 *
 *
 * 管理员接口
 *
 *
 *
 */

 /** 管理员列表接口***/
export function getAdminList(params){
  return fetch({
     url:'/api/admin/admin/list',
     method:'post',
     signValidate:false,
     data:params
  })
}

/**管理员修改**/
export function updateAdminInfo(params){
  return fetch({
     url:'/api/admin/admin/update',
     method:'post',
     signValidate:true,
     data:params
  })
}
/**管理员添加**/
export function addAdminInfo(params){
  return fetch({
     url:'/api/admin/admin/save',
     method:'post',
     signValidate:true,
     data:params
  })
}

/**
 *
 *角色列表
 *
 *
**/
/*角色列表*/
export function getRoleList(params){
  return fetch({
     url:'/api/admin/role/list',
     method:'post',
     signValidate:false,
     data:params
  })
}

/**删除角色**/
export function deleteRoleInfo(params){
  return fetch({
     url:'/api/admin/role/delete',
     method:'post',
     signValidate:true,
     data:params
  })
}

export function getRoleInfo(params){
  return fetch({
     url:'/api/admin/role/get',
     method:'post',
     signValidate:false,
     data:params
  })
}

export function updateRoleInfo(params){
  return fetch({
     url:'/api/admin/role/update',
     method:'post',
     signValidate:true,
     data:params
  })
}

export function addRoleInfo(params){
  return fetch({
     url:'/api/admin/role/save',
     method:'post',
     signValidate:true,
     data:params
  })
}



/*账户绑定信息列表*/
export function getUserAccountList(params){
  return fetch({
     url:'/api/admin/account/list',
     method:'post',
     signValidate:true,
     data:params
  })
}

export function deleteUserAccountInfo(params){
  return fetch({
     url:'/api/admin/account/delete',
     method:'post',
     signValidate:true,
     data:params
  })
}


/*消息系统列表 */
export function getSystemMessageList(params){
  return fetch({
     url:'/api/admin/message/sys_list',
     method:'post',
     signValidate:true,
     data:params
  })
}

/*删除系统消息列表*/
export function deleteSystemMessageInfo(params){
  return fetch({
     url:'/api/admin/message/delete',
     method:'post',
     signValidate:true,
     data:params
  })
}

/*发送系统消息列表*/
export function addSystemMessageInfo(params){
  return fetch({
     url:'/api/admin/message/sendSys',
     method:'post',
     signValidate:true,
     data:params
  })
}

