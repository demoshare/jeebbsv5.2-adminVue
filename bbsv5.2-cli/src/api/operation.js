/*运营api接口 */
import fetch from './api'
import qs from 'qs'

/*友情链接分类列表*/
export function getFriendLinkCtgList(params){
    return fetch({
       url:'/api/admin/friendLinkCtg/list',
       method:'post',
       data:params
    })
  };

/*友情链接分类添加*/
export function addFriendLinkCtg(params){
    return fetch({
       url:'/api/admin/friendLinkCtg/save',
       method:'post',
       signValidate:true,
       data:params
    })
  };

  /*友情链接分类批量删除*/
export function deleteFriendLinkCtgInfo(params){
    return fetch({
       url:'/api/admin/friendLinkCtg/delete',
       method:'post',
       signValidate:true,
       data:params
    })
  };

  /**友情链接分类批量修改**/
  export function friendLinkCtgBatchUpdate(params){
    return fetch({
       url:'/api/admin/friendLinkCtg/update',
       method:'post',
       signValidate:true,
       data:params
    })
  };
  
  /*友情链接列表*/
export function getFriendLinkList(params){
    return fetch({
       url:'/api/admin/friendLink/list',
       method:'post',
       data:params
    })
  };

  /*友情链接信息*/
  export function getFriendLinkInfo(params){
    return fetch({
       url:'/api/admin/friendLink/get',
       method:'post',
       data:params
    })
  };

/*添加友情链接*/
  export function addFriendLinkInfo(params){
    return fetch({
       url:'/api/admin/friendLink/save',
       method:'post',
       signValidate:true,
       data:params
    })
  };
/*修改友情链接*/

export function updateFriendLinkInfo(params){
  return fetch({
     url:'/api/admin/friendLink/update',
     method:'post',
     signValidate:true,
     data:params
  })
};
/*批量保存友情链接排序*/
export function friendLinkBatchUpdate(params){
  return fetch({
     url:'/api/admin/friendLink/priority',
     method:'post',
     signValidate:true,
     data:params
  })
};
  /*删除友情链接列表*/
  export function deleteFriendLinkInfo(params){
    return fetch({
       url:'/api/admin/friendLink/delete',
       method:'post',
       signValidate:true,
       data:params
    })
  };
/*
 * 
 * 
 *广告api
 *  
 * 
 * 
 */


/**广告列表**/
export function getAdvertisingList(params){
  return fetch({
     url:'/api/admin/advertising/list',
     method:'post',
     signValidate:false,
     data:params
  })
};


/*删除广告信息*/
export function deleteAdvertisingInfo(params){
  return fetch({
     url:'/api/admin/advertising/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};


/** 获取广告信息**/
export function getAdvertisingInfo(params){
  return fetch({
     url:'/api/admin/advertising/get',
     method:'post',
     signValidate:false,
     data:params
  })
};

/**修改广告信息**/
export function updateAdvertisingInfo(params){
  return fetch({
     url:'/api/admin/advertising/update',
     method:'post',
     signValidate:true,
     data:params
  })
};
/*添加广告*/
export function addAdvertisingInfo(params){
  return fetch({
     url:'/api/admin/advertising/save',
     method:'post',
     signValidate:true,
     data:params
  })
};
/*获取用户广告余额*/
export function userAdAmount(params){
  return fetch({
     url:'/api/admin/advertising/check_ad_amount',
     method:'post',
     signValidate:true,
     data:params
  })
};

/**广告版位接口*/
/*广告版位信息*/
export function getAdvertisingSpaceInfo(params){
  return fetch({
     url:'/api/admin/advertisingSpace/get',
     method:'post',
     signValidate:false,
     data:params
  })
};
 /*广告版位列表*/
 export function getAdvertisingSpaceList(params){
  return fetch({
     url:'/api/admin/advertisingSpace/list',
     method:'post',
     signValidate:false,
     data:params
  })
};

/*删除广告版位信息*/
export function deleteAdvertisingSpaceInfo(params){
  return fetch({
     url:'/api/admin/advertisingSpace/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*修改广告版位信息*/
export function updateAdvertisingSpaceInfo(params){
  return fetch({
     url:'/api/admin/advertisingSpace/update',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*添加广告版块*/
export function addAdvertisingSpaceInfo(params){
  return fetch({
     url:'/api/admin/advertisingSpace/save',
     method:'post',
     signValidate:true,
     data:params
  })
};
/*
 * 
 * 
 *礼物中心api 
 * 
 */


/*礼物列表*/

export function getGiftList(params){
  return fetch({
     url:'/api/admin/gift/list',
     method:'post',
     signValidate:false,
     data:params
  })
};

/*获取礼物信息*/
export function getGiftInfo(params){
  return fetch({
     url:'/api/admin/gift/get',
     method:'post',
     data:params
  })
};


/*批量删除礼物*/
export function deleteGiftInfo(params){
  return fetch({
     url:'/api/admin/gift/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*批量保存顺序状态*/
export function giftBatchUpdate(params){
  return fetch({
     url:'/api/admin/gift/priority',
     method:'post',
     signValidate:true,
     data:params
  })
};


/*修改礼物信息 */
export function updateGiftInfo(params){
  return fetch({
     url:'/api/admin/gift/update',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*添加礼物*/

export function addGiftInfo(params){
  return fetch({
     url:'/api/admin/gift/save',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*道具列表*/
export function getMagicList(params){
  return fetch({
     url:'/api/admin/magic/list',
     method:'post',
     signValidate:false,
     data:params
  })
};

/**道具信息获取*/
export function getMagicInfo(params){
  return fetch({
     url:'/api/admin/magic/get',
     method:'post',
     data:params
  })
};

/**道具配置信息获取**/
export function getMagicConfig(params){
  return fetch({
     url:'/api/admin/magic/config_get',
     method:'post',
     signValidate:false,
     data:params
  })
};

/*道具批量保存*/
export function magicBatchUpdate(params){
  return fetch({
     url:'/api/admin/magic/o_priority',
     method:'post',
     signValidate:true,
     data:params
  })
};

/**道具更新*/
export function updateMagicInfo(params){
  return fetch({
     url:'/api/admin/magic/update',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*道具配置*/
export function magicConfigUpdate(params){
  return fetch({
     url:'/api/admin/magic/config_update',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*道具赠送*/
export function giveMagic(params){
  return fetch({
     url:'/api/admin/magic/give',
     method:'post',
     signValidate:true,
     data:params
  })
};
/*收费统计*/
export function getOrderList(params){
  return fetch({
     url:'/api/admin/order/charge_list',
     method:'post',
     signValidate:false,
     data:params
  })
};

/*用户账户统计*/
export function getAccountList(params){
  return fetch({
     url:'/api/admin/order/account_list',
     method:'post',
     signValidate:false,
     data:params
  })
};

/**订单流水列表*/
export function getUserOrderList(params){
  return fetch({
     url:'/api/admin/order/user_order_list',
     method:'post',
     signValidate:false,
     data:params
  })
};

/**收益统计**/
export function getIncomeStatisticList(params){
  return fetch({
     url:'/api/admin/data/incomeStatistic_list',
     method:'post',
     signValidate:false,
     data:params
  })
};

/**数据中心**/
export function getForumStatisticList(params){
  return fetch({
     url:'/api/admin/data/forumstatistic_list',
     method:'post',
     signValidate:false,
     data:params
  })
};


/*直播管理*/

export function getLiveList(params){
  return fetch({
     url:'/api/admin/live/list',
     method:'post',
     signValidate:false,
     data:params
  })
};




/**ajax关闭接口**/
export function shutDownLive(params){
  return fetch({
     url:'/api/admin/live/stop',
     method:'post',
     signValidate:true,
     data:params
  })
};
export function startLive(params){
  return fetch({
     url:'/api/admin/live/start',
     method:'post',
     signValidate:true,
     data:params
  })
};
/**退回**/
export function rejectLive(params){
  return fetch({
     url:'/api/admin/live/reject',
     method:'post',
     signValidate:true,
     data:params
  })
};
/**通过**/
export function checkLive(params){
  return fetch({
     url:'/api/admin/live/check',
     method:'post',
     signValidate:true,
     data:params
  })
};


/*已审核列表*/
export function getLiveHostList(params){
  return fetch({
     url:'/api/admin/liveHost/list',
     method:'post',
     signValidate:false,
     data:params
  })
};

export function deleteHostsInfo(params){
  return fetch({
     url:'/api/admin/liveHost/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*主播待审核接口*/
export function getLiveApplyList(params){
  return fetch({
     url:'/api/admin/liveApply/list',
     method:'post',
     signValidate:false,
     data:params
  })
};
/*主播删除接口*/
export function deleteApplyInfo(params){
  return fetch({
     url:'/api/admin/liveApply/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*主讲人申请审核接口*/
export function applyCheck(params){
  return fetch({
     url:'/api/admin/liveApply/check',
     method:'post',
     signValidate:true,
     data:params
  })
};

/**主讲人申请退回接口 */


export function applyReject(params){
  return fetch({
     url:'/api/admin/liveApply/reject',
     method:'post',
     signValidate:true,
     data:params
  })
};


/** 主讲人统计保存排列顺序接口*/
export function liveBatchUpdate(params){
  return fetch({
     url:'/api/admin/liveHost/priority',
     method:'post',
     signValidate:true,
     data:params
  })
};


/**Live配置详情接口 */
export function getLiveConfigInfo(params){
  return fetch({
     url:'/api/admin/live/config_get',
     method:'post',
     signValidate:true,
     data:params
  })
};
export function updateLiveConfigInfo(params){
  return fetch({
     url:'/api/admin/live/config_update',
     method:'post',
     signValidate:true,
     data:params
  })
};
/*提现申请列表*/
export function getAccountDrawList(params){
  return fetch({
     url:'/api/admin/accountDraw/list',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function deletetAccountDrawList(params){
  return fetch({
     url:'/api/admin/accountDraw/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function getAccountPayList(params){
  return fetch({
     url:'/api/admin/accountPay/list',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function deletetAccountPayList(params){
  return fetch({
     url:'/api/admin/accountPay/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function payByWeiXin(params){
  return fetch({
     url:'/api/admin/accountPay/payByWeiXin',
     method:'post',
     signValidate:true,
     data:params
  })
};




