import fetch from './api'
import qs from 'qs'

/*全局设置信息*/
export function getSystemGetInfo(params){
    return fetch({
       url:'/api/admin/site_config/system_get',
       method:'post',
       data:params
    })
  };

/*全局设置修改*/
  export function updateSystemGetInfo(params){
    return fetch({
       url:'/api/admin/site_config/system_update',
       method:'post',
       signValidate:true,
       data:params
    })
  };
  /*站点设置信息*/
  export function getBaseInfo(params){
    return fetch({
       url:'/api/admin/site_config/base_get',
       method:'post',
       data:params
    })
  };
    /*站点设置信息修改*/
  export function updateBaseInfo(params){
    return fetch({
       url:'/api/admin/site_config/base_update',
       method:'post',
       signValidate:true,
       data:params
    })
  };
  

    /*论坛设置信息*/
    export function getBbsInfo(params){
      return fetch({
         url:'/api/admin/bbs_config/get',
         method:'post',
         data:params
      })
    };
  
      /*论坛设置信息修改*/
    export function updateBbsInfo(params){
      return fetch({
         url:'/api/admin/bbs_config/update',
         method:'post',
         signValidate:true,
         data:params
      })
    };
    

     /*登录设置信息*/
     export function getLoginInfo(params){
      return fetch({
         url:'/api/admin/bbs_config/login_get',
         method:'post',
         data:params
      })
    };
  
      /*登录设置信息修改*/
    export function updateLoginInfo(params){
      return fetch({
         url:'/api/admin/bbs_config/login_update',
         method:'post',
         signValidate:true,
         data:params
      })
    };
    
    /*积分设置信息*/
    export function getPointInfo(params){
      return fetch({
         url:'/api/admin/bbs_config/creditExchange_get',
         method:'post',
         data:params
      })
    };
  
      /*积分设置信息修改*/
    export function updatePointInfo(params){
      return fetch({
         url:'/api/admin/bbs_config/creditExchange_update',
         method:'post',
         signValidate:true,
         data:params
      })
    };
    
      /*打赏设置信息*/
      export function getChargeInfo(params){
        return fetch({
           url:'/api/admin/bbs_config/charge_get',
           method:'post',
           data:params
        })
      };
    
        /*打赏设置信息修改*/
      export function updateChargeInfo(params){
        return fetch({
           url:'/api/admin/bbs_config/charge_update',
           method:'post',
           signValidate:true,
           data:params
        })
      };


         /*消息设置信息*/
         export function getMessageInfo(params){
          return fetch({
             url:'/api/admin/bbs_config/message_get',
             method:'post',
             data:params
          })
        };
      
          /*消息设置信息修改*/
        export function updateMessageInfo(params){
          return fetch({
             url:'/api/admin/bbs_config/message_update',
             method:'post',
             signValidate:true,
             data:params
          })
        };

          /*第三方设置信息*/
          export function getApiInfo(params){
            return fetch({
               url:'/api/admin/bbs_config/api_get',
               method:'post',
               data:params
            })
          };
        
          /*第三方设置信息*/
          export function updateApiInfo(params){
            return fetch({
               url:'/api/admin/bbs_config/api_update',
               method:'post',
               signValidate:true,
               data:params
            })
          };

          /*单点设置信息*/
          export function getSsoInfo(params){
            return fetch({
               url:'/api/admin/bbs_config/sso_get',
               method:'post',
               data:params
            })
          };
        
            /*单点设置信息修改*/
          export function updateSsoInfo(params){
            return fetch({
               url:'/api/admin/bbs_config/sso_update',
               method:'post',
               signValidate:true,
               data:params
            })
          };



 /*模型列表信息*/
 export function getModelList(params){
  return fetch({
     url:'/api/admin/bbs_config/item_list',
     method:'post',
     data:params
  })
};

  /*模型设置信息修改*/
  export function deleteModelInfo(params){
    return fetch({
       url:'/api/admin/bbs_config/item_delete',
       method:'post',
       signValidate:true,
       data:params
    })
  };

  export function modelBatchUpdate(params){
    return fetch({
       url:'/api/admin/bbs_config/item_priority',
       method:'post',
       signValidate:true,
       data:params
    })
  };


  export function getModelInfo(params){
    return fetch({
       url:'/api/admin/bbs_config/item_get',
       method:'post',
       signValidate:true,
       data:params
    })
  };

  export function updateModelInfo(params){
    return fetch({
       url:'/api/admin/bbs_config/item_update',
       method:'post',
       signValidate:true,
       data:params
    })
  };

  export function addModelInfo(params){
    return fetch({
       url:'/api/admin/bbs_config/item_save',
       method:'post',
       signValidate:true,
       data:params
    })
  };
  
  /*广告设置信息*/
  export function getAdInfo(params){
    return fetch({
       url:'/api/admin/bbs_config/ad_get',
       method:'post',
       data:params
    })
  };

    /*广告设置信息修改*/
  export function updateAdInfo(params){
    return fetch({
       url:'/api/admin/bbs_config/ad_update',
       method:'post',
       signValidate:true,
       data:params
    })
  };

  

  /**接口*/
  export function getWebServiceList(params){
    return fetch({
       url:'/api/admin/webservice/list',
       method:'post',
       data:params
    })
  };
  
   export function deleteWebServiceInfo(params){
    return fetch({
       url:'/api/admin/webservice/delete',
       method:'post',
       signValidate:true,
       data:params
    })
  };

  export function getWebServiceInfo(params){
    return fetch({
       url:'/api/admin/webservice/get',
       method:'post',
       signValidate:true,
       data:params
    })
  };

  export function updateWebServiceInfo(params){
    return fetch({
       url:'/api/admin/webservice/update',
       method:'post',
       signValidate:true,
       data:params
    })
  };

  export function addWebServiceInfo(params){
    return fetch({
       url:'/api/admin/webservice/save',
       method:'post',
       signValidate:true,
       data:params
    })
  };
          


 /**接口用户*/
 export function getWebServiceAuthList(params){
  return fetch({
     url:'/api/admin/webserviceAuth/list',
     method:'post',
     data:params
  })
};

 export function deleteWebServiceAuthInfo(params){
  return fetch({
     url:'/api/admin/webserviceAuth/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function getWebServiceAuthInfo(params){
  return fetch({
     url:'/api/admin/webserviceAuth/get',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function updateWebServiceAuthInfo(params){
  return fetch({
     url:'/api/admin/webserviceAuth/update',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function addWebServiceAuthInfo(params){
  return fetch({
     url:'/api/admin/webserviceAuth/save',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*限制id*/
export function getBbslimitList(params){
  return fetch({
     url:'/api/admin/bbslimit/list',
     method:'post',
     data:params
  })
};

 export function deleteBbslimitInfo(params){
  return fetch({
     url:'/api/admin/bbslimit/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function getBbslimitInfo(params){
  return fetch({
     url:'/api/admin/bbslimit/get',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function updateBbslimitInfo(params){
  return fetch({
     url:'/api/admin/bbslimit/update',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function addBbslimitInfo(params){
  return fetch({
     url:'/api/admin/bbslimit/save',
     method:'post',
     signValidate:true,
     data:params
  })
};




/*api接口*/
export function getApiInfoList(params){
  return fetch({
     url:'/api/admin/apiInfo/list',
     method:'post',
     data:params
  })
};

 export function deleteApiInfoInfo(params){
  return fetch({
     url:'/api/admin/apiInfo/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function getApiInfoInfo(params){
  return fetch({
     url:'/api/admin/apiInfo/get',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function updateApiInfoInfo(params){
  return fetch({
     url:'/api/admin/apiInfo/update',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function addApiInfoInfo(params){
  return fetch({
     url:'/api/admin/apiInfo/save',
     method:'post',
     signValidate:true,
     data:params
  })
};

/*-----*/
export function getApiAccountList(params){
  return fetch({
     url:'/api/admin/apiAccount/list',
     method:'post',
     data:params
  })
};

 export function deleteApiAccountInfo(params){
  return fetch({
     url:'/api/admin/apiAccount/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function getApiAccountInfo(params){
  return fetch({
     url:'/api/admin/apiAccount/get',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function updateApiAccountInfo(params){
  return fetch({
     url:'/api/admin/apiAccount/update',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function addApiAccountInfo(params){
  return fetch({
     url:'/api/admin/apiAccount/save',
     method:'post',
     signValidate:true,
     data:params
  })
};


/**api接口记录**/
export function getApiRecordList(params){
  return fetch({
     url:'/api/admin/apiRecord/list',
     method:'post',
     data:params
  })
};

 export function deleteApiRecordInfo(params){
  return fetch({
     url:'/api/admin/apiRecord/delete',
     method:'post',
     signValidate:true,
     data:params
  })
};

/**独立密码验证
 * params:pwd
 */

export function validateApiPwd(params){
  return fetch({
     url:'/api/admin/config_api_pwd/validate',
     method:'post',
     signValidate:true,
     data:params
  })
};

export function updateApiPwd(params){
  return fetch({
     url:'/api/admin/config_api_pwd/update',
     method:'post',
     signValidate:true,
     data:params
  })
};


