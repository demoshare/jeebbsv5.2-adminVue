 /*论坛api接口*/
import fetch from './api'
import qs from 'qs'
  /*分区版块列表*/
  export function listGroupByCategory(params){
    return fetch({
       url:'/api/admin/forum/listGroupByCategory',
       method:'post',
       data:params
    })
  };
   /* 分区列表信息*/
   export function getCategoryList(params){
    return fetch({
       url:'/api/admin/category/list',
       method:'post',
       data:params
    })
  };
 /*版块列表信息*/
 export function getFourmList(params){
  return fetch({
     url:'/api/admin/forum/list',
     method:'post',
     data:params
  })
}
/*分区版块修改*/
export function saveList(params){
    return fetch({
       url:'/api/admin/forum/batchupdate',
       method:'post',
       signValidate:true,
       data:params
    })
  };
  /*删除分区*/
  export function deleteCategory(params){
    return fetch({
       url:'/api/admin/category/delete',
       method:'post',
       signValidate:true,
       data:params
    })
  }
  /*删除版块*/
  export function deleteForum(params){
    return fetch({
       url:'/api/admin/forum/delete',
       method:'post',
       signValidate:true,
       data:params
    })
  }
  /*分区排序接口*/
  export function categoryListUpdate(params){
    return fetch({
       url:'/api/admin/category/o_priority_update',
       method:'post',
       signValidate:true,
       data:params
    })
  }
    /*版块排序接口*/
    export function forumListUpdate(params){
      return fetch({
         url:'/api/admin/forum/o_priority_update',
         method:'post',
         signValidate:true,
         data:params
      })
    }

    /*获取版块所有的字段信息*/
    export function getFourm(params){
      return fetch({
         url:'/api//admin/forum/get',
         method:'post',
         data:params
      })
    }
   /*修改版块字段信息*/
   export function updateFourms(params){
    return fetch({
       url:'/api/admin/forum/update',
       method:'post',
       signValidate:true,
       data:params
    })
  }
/*
 *
 * 话题
 *
 *
 */


export function getTopicTypeTree(params){
  return fetch({
     url:'/api/admin/topicType/tree',
     method:'post',
     signValidate:false,
     data:params
  })
}
 /*主题分类列表*/
 export function getTopicTypeList(params){
  return fetch({
     url:'/api/admin/topicType/list',
     method:'post',
     signValidate:false,
     data:params
  })
}

/*主题分类信息*/
export function getTopicTypeInfo(params){
  return fetch({
     url:'/api/admin/topicType/get',
     method:'post',
     signValidate:false,
     data:params
  })
}
/*删除主题分类*/
export function deleteTopicTypeInfo(params){
  return fetch({
     url:'/api/admin/topicType/delete',
     method:'post',
     signValidate:true,
     data:params
  })
}

/*修改主题分类信息 */
export function updateTopicTypeInfo(params){
  return fetch({
     url:'/api/admin/topicType/update',
     method:'post',
     signValidate:true,
     data:params
  })
}
/*添加主题分类信息 */
export function addTopicTypeInfo(params){
  return fetch({
     url:'/api/admin/topicType/save',
     method:'post',
     signValidate:true,
     data:params
  })
}

/*获取敏感词列表*/
export function getSensitivityList(params){
  return fetch({
     url:'/api/admin/sensitivity/list',
     method:'post',
     signValidate:false,
     data:params
  })
}
/*删除*/
export function deleteSensitivityInfo(params){
  return fetch({
     url:'/api/admin/sensitivity/delete',
     method:'post',
     signValidate:true,
     data:params
  })
}

/*快速添加*/
export function addFastSensitivity(params){
  return fetch({
     url:'/api/admin/sensitivity/save',
     method:'post',
     signValidate:true,
     data:params
  })
}
/*批量修改*/
export function sensitivityBatchUpdate(params){
  return fetch({
     url:'/api/admin/sensitivity/batch_update',
     method:'post',
     signValidate:true,
     data:params
  })
}
/*批量添加 */
export function sensitivityBatchSave(params){
  return fetch({
     url:'/api/admin/sensitivity/batch_save',
     method:'post',
     signValidate:true,
     data:params
  })
}

/*
*
*
用户举报管理
*
*/
export function getReportList(params){
  return fetch({
     url:'/api/admin/report/list',
     method:'post',
     data:params
  })
}

/*获取用户举报信息*/
export function getReportInfo(params){
  return fetch({
     url:'/api/admin/report/get',
     method:'post',
     data:params
  })
}

/*处理用户举报*/

export function process(params){
  return fetch({
     url:'/api/admin/report/process',
     signValidate:true,
     method:'post',
     data:params
  })
}

/*删除用户举报 */
export function deleteReportInfo(params){
  return fetch({
     url:'/api/admin/report/delete',
     signValidate:true,
     method:'post',
     data:params
  })
}











