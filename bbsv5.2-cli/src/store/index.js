import Vue from 'vue'
import Vuex from 'vuex'
import * as user from '../api/user'
import { getRand } from "../untils/random";
import { routes, ansycRoutes } from '../routers/router'
import router from '../routers/index'
Vue.use(Vuex)
let href = location.href;//截取URL地址区分是否带项目路径
let localHref = href.substring(0, href.indexOf('/jeeadmin'));
let apiUrl = '';
apiUrl = process.env.Base_URL == '' ? localHref : process.env.Base_URL;//判断是否分离部署
const state = {
    appId: '7166912116544627',
    aesKey: 'wKIFyACLEUvHnSIT',
    ivKey: '1yTSp6TP47uP12RK',
    appKey: 'vDnwyGf4Ej8eCcqLkhjaHSmav2TAXGVa',
    baseUrl: apiUrl,
    upLoadUrl: '/api/admin/upload/o_upload',
    resourceUpLoad: '/api/admin/resource/upload',
    templateUpLoad: '/api/admin/template/upload',
    importTpl: '/api/admin/template/importTpl',
    sessionKey: '',//sessionkey
    authorState: false,//登录状态
    userInfo: {},
    routers:routes,//管理员免权限
    addRouters:[],
    perms:false,//权限
    sort: [{
        value: '0',
        label: '排序'
    }, {
        value: '1',
        label: '今日活跃度降序'
    }, {
        value: '2',
        label: '今日活跃度升序'
    }, {
        value: '3',
        label: '本周活跃度降序'
    }, {
        value: '4',
        label: '本周活跃度升序'
    }, {
        value: '5',
        label: '本月活跃度降序'
    },
    {
        value: '6',
        label: '本月活跃度升序'
    }, {
        value: '7',
        label: '今年活跃度降序'
    }, {
        value: '8',
        label: '今年活跃度升序'
    }, {
        value: '9',
        label: '积分降序'
    }, {
        value: '10',
        label: '积分升序'
    }, {
        value: '11',
        label: '威望降序'
    }, {
        value: '12',
        label: '威望升序'
    }],
    lastLoginDay: [{
        value: '0',
        label: '所有'
    }, {
        value: '3',
        label: '三天'
    }, {
        value: '7',
        label: '一周'
    }, {
        value: '30',
        label: '一个月'
    }, {
        value: '90',
        label: '三个月'
    }
    ]
}

/*
 * 
 * @param {异步路由表} ansycRoutes 
 * @param {数据库权限拉取} perms 
 */
function getansycRoutes(tmpRoutes, perms) {//处理角色权限
   const result = tmpRoutes.filter(route => {
        
        if (perms.indexOf(route.meta.role) != -1) {
            if (route.children != undefined) {
                route.children = getansycRoutes(route.children, perms);
            }
            return true;
        }
        return false;
    }
    )
    return result
}

const mutations = {
    SET_ROUTERS: (state, asRouters) => {//筛选路由表
        state.routers=routes.concat(asRouters);
        state.addRouters=asRouters;
        state.perms=true
    },
    CLEAR_ROUTERS: (state) => {//刷新路由表
        localStorage.clear();
        state.perms=false;
        state.routers=routes,//管理员免权限
        state.addRouters=[]
        window.location.reload();
    }
}
const actions = {
    setRouters({ commit }) {//触发权限路由
        return new Promise(resolve => {
            let sessionKey = localStorage.getItem('sessionKey');
            let params = { sessionKey: sessionKey, nonce_str: getRand() }    
            user.getPerms(params).then(res => {
                let perms = res.body.perms;//权限
              
                let asRouters;
                if (perms == '*') {
                    asRouters = ansycRoutes;
                } else {
                    asRouters = getansycRoutes(ansycRoutes,perms);//递归过滤
                   
                }
                commit('SET_ROUTERS',asRouters);
                resolve();
            })
        })
    },
    clearRouters({ commit }) {
        return new Promise(resolve=>{
            commit('CLEAR_ROUTERS');
            resolve();     
        })
       
    }
}


export default new Vuex.Store({
    state,
    mutations,
    actions
})