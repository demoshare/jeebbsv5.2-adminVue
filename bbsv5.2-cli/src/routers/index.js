import Vue from 'vue'//引入vue
import VueRouter from 'vue-router'//引入路由
import {routes} from './router'//路由配置文
Vue.use(VueRouter)
export default new VueRouter({
      mode:'hash',
    routes//路由文件配置
  })