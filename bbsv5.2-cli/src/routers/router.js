/**
 * Created by admin on 2017/8/24.
 */
import header from '../components/header.vue'
import body from '../components/body.vue'
import error from '../components/404.vue'
import work from '../views/work.vue'
const login = resolve => {
  require(['../components/login.vue'], resolve)
};
//  const userInfo =resolve => {require(['../components/userInfo.vue'], resolve)};
/*论坛*/
const fourmList = resolve => {
  require(['../views/bbs/fourm/fourm.vue'], resolve)
};
const fourmEdit = resolve => {
  require(['../views/bbs/fourm/fourmEdit.vue'], resolve)
};
const topicTypeList = resolve => {
  require(['../views/bbs/topicType/list.vue'], resolve)
};
const topicTypeEdit = resolve => {
  require(['../views/bbs/topicType/edit.vue'], resolve)
};
const topicTypeTree = resolve => {
  require(['../views/bbs/topicType/tree.vue'], resolve)
};
const sensitivityList = resolve => {
  require(['../views/bbs/sensitivity/list.vue'], resolve)
};
const sensitivityEdit = resolve => {
  require(['../views/bbs/sensitivity/edit.vue'], resolve)
};
const reportList = resolve => {
  require(['../views/bbs/report/list.vue'], resolve)
}; //用户举报列表
const reportEdit = resolve => {
  require(['../views/bbs/report/edit.vue'], resolve)
}; //用户举报修改
/*运营*/
const friendLinkCtgList = resolve => {
  require(['../views/operation/friendLinkCtg/list.vue'], resolve)
}; //友情链接类别
const friendLinkList = resolve => {
  require(['../views/operation/friendLink/list.vue'], resolve)
}; //友情链接列表
const friendLinkEdit = resolve => {
  require(['../views/operation/friendLink/edit.vue'], resolve)
}; //友情链接添加修改
const advertisingList = resolve => {
  require(['../views/operation/adManage/advertising/list.vue'], resolve)
}; ////广告列表
const advertisingEdit = resolve => {
  require(['../views/operation/adManage/advertising/edit.vue'], resolve)
}; //////广告修改
const advertisingAdd = resolve => {
  require(['../views/operation/adManage/advertising/add.vue'], resolve)
}; ////广告添加
const advertisingSpaceList = resolve => {
  require(['../views/operation/adManage/advertisingSpace/list.vue'], resolve)
};
//广告版块列表
const advertisingSpaceEdit = resolve => {
  require(['../views/operation/adManage/advertisingSpace/edit.vue'], resolve)
}; //友情链接添加修改
//广告版块添加修改

const giftList = resolve => {
  require(['../views/operation/gift/list.vue'], resolve)
}; //礼物中心列表
const giftEdit = resolve => {
  require(['../views/operation/gift/edit.vue'], resolve)
}; //礼物修改
const chargeList = resolve => {
  require(['../views/operation/order/charge/list.vue'], resolve)
}; ////收费统计
const accountList = resolve => {
  require(['../views/operation/order/account/list.vue'], resolve)
}; //礼物中心列表
const userOrderList = resolve => {
  require(['../views/operation/order/userOrder/list.vue'], resolve)
}; //礼物中心列表
const incomeStatisticList = resolve => {
  require(['../views/operation/order/incomeStatistic/list.vue'], resolve)
}; //礼物中心列表
const forumStatisticList = resolve => {
  require(['../views/operation/order/forumStatistic/list.vue'], resolve)
}; //礼物中心列表


const magicList = resolve => {
  require(['../views/operation/magic/magicList/list.vue'], resolve)
}; //道具中心列表
const magicEdit = resolve => {
  require(['../views/operation/magic/magicList/edit.vue'], resolve)
}; //道具中心修改
const magicConfig = resolve => {
  require(['../views/operation/magic/magicConfig.vue'], resolve)
}; //道具中心修改
const selectUser = resolve => {
  require(['../views/operation/magic/user.vue'], resolve)
}; //道具选择用户
const giveMagic = resolve => {
  require(['../views/operation/magic/giveMagic.vue'], resolve)
}; //道具选择用户
const liveList = resolve => {
  require(['../views/operation/lives/live/list.vue'], resolve)
}; //直播管理列表
const liveConfig = resolve => {
  require(['../views/operation/lives/live/edit.vue'], resolve)
}; //直播配置
const hostsList = resolve => {
  require(['../views/operation/lives/hosts/list.vue'], resolve)
}; //主播管理列表
const accountDrawList = resolve => {
  require(['../views/operation/accountDraw/list.vue'], resolve)
}; //提现列表
const accountPayList = resolve => {
  require(['../views/operation/accountPay/list.vue'], resolve)
}; //转账列表
const accountPayEdit = resolve => {
  require(['../views/operation/accountDraw/accountPay.vue'], resolve)
}; //转账列表
/*用户*/
const userGruop = resolve => {
  require(['../views/users/userGroup/usergroup.vue'], resolve)
}; //礼物中心列表
const userGruopList = resolve => {
  require(['../views/users/userGroup/userGroupList.vue'], resolve)
}; //礼物中心列表
const userGruopEdit = resolve => {
  require(['../views/users/userGroup/edit.vue'], resolve)
}; //礼物中心列表
const userList = resolve => {
  require(['../views/users/user/list.vue'], resolve)
}; //礼物中心列表
const userEdit = resolve => {
  require(['../views/users/user/edit.vue'], resolve)
}; //
const userAdd = resolve => {
  require(['../views/users/user/add.vue'], resolve)
}; //
const adminList = resolve => {
  require(['../views/users/admin/list.vue'], resolve)
}; //
const adminEdit = resolve => {
  require(['../views/users/admin/edit.vue'], resolve)
}; //
const adminAdd = resolve => {
  require(['../views/users/admin/add.vue'], resolve)
}; //
const officialList = resolve => {
  require(['../views/users/official/list.vue'], resolve)
}; //
const officialEdit = resolve => {
  require(['../views/users/official/edit.vue'], resolve)
}; //
const officialAdd = resolve => {
  require(['../views/users/official/add.vue'], resolve)
}; //
const roleList = resolve => {
  require(['../views/users/role/list.vue'], resolve)
}; //
const roleEdit = resolve => {
  require(['../views/users/role/edit.vue'], resolve)
}; //
const userAccountList = resolve => {
  require(['../views/users/account/list.vue'], resolve)
}; //三方用户绑定
const systemMessageList = resolve => {
  require(['../views/users/systemMessage/list.vue'], resolve)
}; //系统消息
const systemMessageAdd = resolve => {
  require(['../views/users/systemMessage/add.vue'], resolve)
}; //系统消息
/*界面管理*/
//
//列表
//修改添加
//重命名

const resourceTree = resolve => {
  require(['../views/interface/resource/tree.vue'], resolve)
}; //
const resourceList = resolve => {
  require(['../views/interface/resource/list.vue'], resolve)
}; //
const resourceEdit = resolve => {
  require(['../views/interface/resource/edit.vue'], resolve)
}; //
const resourceReName = resolve => {
  require(['../views/interface/resource/rename.vue'], resolve)
}; //

const templateTree = resolve => {
  require(['../views/interface/template/tree.vue'], resolve)
}; //
const templateList = resolve => {
  require(['../views/interface/template/list.vue'], resolve)
}; //
const templateEdit = resolve => {
  require(['../views/interface/template/edit.vue'], resolve)
}; //
const templateReName = resolve => {
  require(['../views/interface/template/rename.vue'], resolve)
}; //
const templateSetting = resolve => {
  require(['../views/interface/template/setting.vue'], resolve)
}; //

/*全局*/
//全局设置
//站点设置
//论坛设置
//登录设置
//积分设置
//打赏设置
//打赏设置
//弟三方登录设置
//单点登录设置
//模型列表
//修改添加模型
//单点登录设置
//接口管理列表
//接口修改添加模型
//接口管理列表
//接口修改添加模型
//限制id
//限制id
//api接口管理
//api接口修改
const siteConfig = resolve => {
  require(['../views/systemConfig/siteConfig/edit.vue'], resolve)
}; //
const baseConfig = resolve => {
  require(['../views/systemConfig/baseConfig/edit.vue'], resolve)
}; //
const bbsConfig = resolve => {
  require(['../views/systemConfig/bbsConfig/edit.vue'], resolve)
}; //
const loginConfig = resolve => {
  require(['../views/systemConfig/loginConfig/edit.vue'], resolve)
}; //
const pointConfig = resolve => {
  require(['../views/systemConfig/pointConfig/edit.vue'], resolve)
}; //
const chargeConfig = resolve => {
  require(['../views/systemConfig/chargeConfig/edit.vue'], resolve)
}; //
const messageConfig = resolve => {
  require(['../views/systemConfig/messageConfig/edit.vue'], resolve)
}; //
const apiConfig = resolve => {
  require(['../views/systemConfig/thirdLoginConfig/edit.vue'], resolve)
}; //
const ssoConfig = resolve => {
  require(['../views/systemConfig/ssoConfig/edit.vue'], resolve)
}; //
const modelConfig = resolve => {
  require(['../views/systemConfig/modelConfig/list.vue'], resolve)
}; //
const modelConfigEdit = resolve => {
  require(['../views/systemConfig/modelConfig/edit.vue'], resolve)
}; //
const adConfig = resolve => {
  require(['../views/systemConfig/adConfig/edit.vue'], resolve)
}; //
const webserviceConfig = resolve => {
  require(['../views/systemConfig/webserviceConfig/list.vue'], resolve)
}; //
const webserviceConfigEdit = resolve => {
  require(['../views/systemConfig/webserviceConfig/edit.vue'], resolve)
}; //
const webserviceAuthConfig = resolve => {
  require(['../views/systemConfig/webserviceAuthConfig/list.vue'], resolve)
}; //
const webserviceAuthEdit = resolve => {
  require(['../views/systemConfig/webserviceAuthConfig/edit.vue'], resolve)
}; //
const bbsLimit = resolve => {
  require(['../views/systemConfig/bbsLimit/list.vue'], resolve)
}; //
const bbsLimitEdit = resolve => {
  require(['../views/systemConfig/bbsLimit/edit.vue'], resolve)
}; //
const apiInfoList = resolve => {
  require(['../views/systemConfig/apiInfo/list.vue'], resolve)
}; //
const apiInfoEdit = resolve => {
  require(['../views/systemConfig/apiInfo/edit.vue'], resolve)
}; //

const apiAccountList = resolve => {
  require(['../views/systemConfig/apiAccount/list.vue'], resolve)
}; //
const apiAccountEdit = resolve => {
  require(['../views/systemConfig/apiAccount/edit.vue'], resolve)
}; //
const apiAccountUpdate = resolve => {
  require(['../views/systemConfig/apiAccount/updatePwd.vue'], resolve)
}; //

const apiRecordList = resolve => {
  require(['../views/systemConfig/apiRecord/list.vue'], resolve)
}; //


export const routes = [{
  path: '/login',
  name: '登录',
  component: login,
  hidden: true
},
{
  path: '/error',
  name: '服务器关闭',
  component: error,
  hidden: true
},
{
  path: '/',
  component: header,
  iconCls: 'bbs-iconfontdesktop',
  leaf: true, //只有一个节点
  redirect: '/work',
  children: [{
    path: '/work',
    name: '工作台',
    component: work,
    hidden: true
  }]
}
];
export const ansycRoutes = [{
  path: '/bbs',
  name: '论坛',
  component: header,
  redirect: '/fourm',
  iconCls: 'bbs-appstoreo',
  meta: {
    role: 'bbsparent'
  },
  children: [{
    path: '/fourm',
    name: '版块管理',
    component: body,
    meta: {
      role: 'fourmlist'
    },
    children: [{
      path: '/',
      name: '版块列表',
      component: fourmList,
      meta: {
        role: 'fourmlist'
      },
    },
    {
      path: '/fourm-edit',
      name: '版块修改',
      component: fourmEdit,
      hidden: true,
      meta: {
        role: 'fourmedit'
      }
    }
    ]
  },
  {
    path: '/topictypelist',
    meta: {
      role: 'topictypelist'
    },
    name: '话题管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'topictypelist'
      },
      component: topicTypeTree,
      tree: true,
      children: [{
        path: '/topictypelist',
        meta: {
          role: 'topictypelist'
        },
        name: '话题管理列表',
        component: topicTypeList,
        hidden: true
      }
      ]
    },
    {
      path: '/topictypeadd',
      meta: {
        role: 'topictypeadd'
      },
      name: '添加话题',
      component: topicTypeEdit,
      hidden: true
    },
    {
      path: '/topictypeedit',
      meta: {
        role: 'topictypeedit'
      },
      name: '修改话题信息',
      component: topicTypeEdit,
      hidden: true
    }
    ]
  },
  {
    path: '/sensitivitylist',
    name: '敏感词管理',
    meta: {
      role: 'sensitivitylist'
    },
    component: body,
    children: [{
      path: '/',
      name: '敏感词管理列表',
      meta: {
        role: 'sensitivitylist'
      },
      component: sensitivityList
    },
    {
      path: '/sensitivityedit',
      meta: {
        role: 'sensitivityadd'
      },
      name: '批量添加敏感词',
      component: sensitivityEdit,
      hidden: true
    }
    ]
  },
  {
    path: '/reportlist',
    meta: {
      role: 'reportlist'
    },
    name: '用户举报管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'reportlist'
      },
      name: '用户举报列表',
      component: reportList
    },
    {
      path: '/reportedit',
      meta: {
        role: 'reportedit'
      },
      name: '用户举报详情',
      component: reportEdit,
      hidden: true
    }
    ]
  }
  ]
},
{
  path: '/',
  meta: {
    role: 'operation'
  },
  name: '运营',
  component: header,
  iconCls: 'bbs-barschart',
  children: [{
    path: '/friendlinkctg',
    meta: {
      role: 'friendlinkctglist'
    },
    name: '友情链接类别管理',
    component: body,
    hidden:true,
    children: [{
      path: '/',
      meta: {
        role: 'friendlinkctglist'
      },
      component: friendLinkCtgList
    }]
  },
  {
    path: '/friendlinklist',
    meta: {
      role: 'friendlinklist'
    },
    name: '友情链接管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'friendlinklist'
      },
      component: friendLinkList
    }, {
      path: '/friendlinkadd',
      meta: {
        role: 'friendlinkadd'
      },
      name: '添加友情链接',
      component: friendLinkEdit,
      hidden: true
    },
    {
      path: '/friendlinkedit',
      meta: {
        role: 'friendlinkedit'
      },
      name: '修改友情链接',
      component: friendLinkEdit,
      hidden: true
    }
    ]
  }, {
    path: '/advertisinglist',
    meta: {
      role: 'advertisinglist'
    },
    name: '广告管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'advertisinglist'
      },
      name: '广告列表',
      component: advertisingList,
      hidden: true
    },
    {
      path: '/advertisingedit',
      meta: {
        role: 'advertisingedit'
      },
      name: '广告修改',
      component: advertisingEdit,
      hidden: true
    },
    {
      path: '/advertisingadd',
      meta: {
        role: 'advertisingadd'
      },
      name: '广告添加',
      component: advertisingAdd,
      hidden: true
    },
    {
      path: '/advertisingspacelist',
      meta: {
        role: 'advertisingspacelist'
      },
      name: '广告版位列表',
      component: advertisingSpaceList,
      hidden: true
    },
    {
      path: '/advertisingspaceedit',
      meta: {
        role: 'advertisingspaceedit'
      },
      name: '广告版位修改',
      component: advertisingSpaceEdit,
      hidden: true
    },
    {
      path: '/advertisingspaceadd',
      meta: {
        role: 'advertisingspaceadd'
      },
      name: '广告版位添加',
      component: advertisingSpaceEdit,
      hidden: true
    }
    ]
  },
  {
    path: '/giftlist',
    meta: {
      role: 'giftlist'
    },
    name: '礼物中心',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'giftlist'
      },
      component: giftList
    }, {
      path: '/giftadd',
      meta: {
        role: 'giftadd'
      },
      name: '添加礼物',
      component: giftEdit,
      hidden: true
    },
    {
      path: '/giftedit',
      meta: {
        role: 'giftedit'
      },
      name: '修改礼物信息',
      component: giftEdit,
      hidden: true
    }
    ]
  },
  {
    path: '/magiclist',
    meta: {
      role: 'magiclist'
    },
    name: '道具中心',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'magiclist'
      },
      component: magicList
    },
    {
      path: '/magicedit',
      meta: {
        role: 'magicedit'
      },
      name: '修改道具信息',
      component: magicEdit,
      hidden: true
    }, {
      path: '/givemagic',
      meta: {
        role: 'givemagic'
      },
      name: '道具赠送-选择道具',
      component: giveMagic,
      hidden: true
    },

    {
      path: '/selectuser',
      meta: {
        role: 'selectuser'
      },
      name: '道具赠送-选择用户',
      component: selectUser,
      hidden: true
    },
    {
      path: '/magicconfig',
      meta: {
        role: 'magicconfig'
      },
      name: '道具中心配置',
      component: magicConfig,
      hidden: true
    }
    ]
  }, {
    path: '/accountdrawlist',
    meta: {
      role: 'accountdrawlist'
    },
    name: '提现管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'accountdrawlist'
      },
      component: accountDrawList
    },
    {
      name: '提现',
      meta: {
        role: 'accountpayedit'
      },
      path: '/accountpayedit',
      component: accountPayEdit,
      hidden: true
    },
    ]
  }, {
    path: '/accountpaylist',
    meta: {
      role: 'accountpaylist'
    },
    name: '转账管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'accountpaylist'
      },
      component: accountPayList
    }]
  }, {
    path: '/livelist',
    meta: {
      role: 'livelist'
    },
    name: '直播管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'livelist'
      },
      component: liveList,

    }, {
      path: '/liveconfig',
      meta: {
        role: 'liveconfig'
      },
      name: '直播配置',
      hidden: true,
      meta: {
        role: 'liveconfig'
      },
      component: liveConfig,
    }]
  }, {
    path: '/hostlist',
    meta: {
      role: 'hostlist'
    },
    name: '主播管理',
    component: body,
    hidden: true,
    children: [{
      path: '/',
      meta: {
        role: 'hostlist'
      },
      component: hostsList,
    }]
  },
  {
    path: '/chargelist',
    meta: {
      role: 'chargelist'
    },
    name: '收费统计',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'chargelist'
      },
      component: chargeList
    }]
  },
  {
    path: '/accountlist',
    meta: {
      role: 'accountlist'
    },
    name: '用户账户统计',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'accountlist'
      },
      component: accountList
    }]
  },
  {
    path: '/userorderlist',
    meta: {
      role: 'userorderlist'
    },
    name: '订单流水',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'userorderlist'
      },
      component: userOrderList
    }]
  },
  {
    path: '/incomeStatisticlist',
    name: '收益统计',
    meta: {
      role: 'incomeStatisticlist'
    },
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'incomeStatisticlist'
      },
      component: incomeStatisticList
    }]
  },
  {
    path: '/forumstatisticlist',
    meta: {
      role: 'forumstatisticlist'
    },
    name: '数据中心',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'forumstatisticlist'
      },
      component: forumStatisticList
    }]
  }
  ]
},
{
  path: '/',
  name: '用户',
  meta: {
    role: 'user'
  },
  component: header,
  iconCls: 'bbs-user',
  children: [{
    path: '/usergroup',
    meta: {
      role: 'usergroup'
    },
    name: '用户组管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'usergroup'
      },
      component: userGruop
    },
    {
      path: '/usergrouplist',
      meta: {
        role: 'usergroup'
      },
      name: '用户组管理列表',
      component: userGruopList,
      hidden: true
    },
    {
      path: '/usergroupadd',
      meta: {
        role: 'usergroupadd'
      },
      name: '添加用户头衔',
      component: userGruopEdit,
      hidden: true
    },
    {
      path: '/usergroupedit',
      meta: {
        role: 'usergroupedit'
      },
      name: '修改用户头衔',
      component: userGruopEdit,
      hidden: true
    }
    ]
  }, {
    path: '/userlist',
    meta: {
      role: 'userlist'
    },
    name: '用户管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'userlist'
      },
      component: userList
    },
    {
      path: '/useradd',
      meta: {
        role: 'useradd'
      },
      name: '添加用户',
      component: userAdd,
      hidden: true
    },
    {
      path: '/useredit',
      meta: {
        role: 'useredit'
      },
      name: '修改用户信息',
      component: userEdit,
      hidden: true
    }
    ]
  },
  {
    path: '/adminlist',
    meta: {
      role: 'adminlist'
    },
    name: '管理员管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'adminlist'
      },
      component: adminList
    },
    {
      path: '/adminadd',
      meta: {
        role: 'adminadd'
      },
      name: '添加管理员',
      component: adminAdd,
      hidden: true
    },
    {
      path: '/adminedit',
      meta: {
        role: 'adminedit'
      },
      name: '修改管理员信息',
      component: adminEdit,
      hidden: true
    }
    ]
  }, {
    path: '/officiallist',
    meta: {
      role: 'officiallist'
    },
    name: '官网团队账户',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'officiallist'
      },
      component: officialList
    },
    {
      path: '/officialadd',
      meta: {
        role: 'officialadd'
      },
      name: '添加官网账户',
      component: officialAdd,
      hidden: true
    },
    {
      path: '/officialedit',
      meta: {
        role: 'officialedit'
      },
      name: '修改官网账户信息',
      component: officialEdit,
      hidden: true
    }
    ]
  }, {
    path: '/useraccountlist',
    meta: {
      role: 'useraccountlist'
    },
    name: '账户绑定',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'useraccountlist'
      },
      component: userAccountList
    }]
  }, {
    path: '/rolelist',
    meta: {
      role: 'rolelist'
    },
    name: '角色管理',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'rolelist'
      },
      component: roleList
    },
    {
      path: '/roleadd',
      meta: {
        role: 'roleadd'
      },
      name: '添加角色',
      component: roleEdit,
      hidden: true
    },
    {
      path: '/roleedit',
      meta: {
        role: 'roleedit'
      },
      name: '修改角色信息',
      component: roleEdit,
      hidden: true
    }
    ]
  }, {
    path: '/systemmessagelist',
    meta: {
      role: 'systemmessagelist'
    },
    name: '系统消息',
    component: body,
    children: [{
      path: '/',
      meta: {
        role: 'systemmessagelist'
      },
      component: systemMessageList
    }, {
      path: '/systemmessageadd',
      meta: {
        role: 'systemmessageadd'
      },
      name: '发送系统消息',
      component: systemMessageAdd,
      hidden: true
    }]
  }
  ]
}, {
  path: '/',
  name: '界面',
  meta: {
    role: 'interface'
  },
  component: header,
  iconCls: 'bbs-windowso',
  children: [
    {
      path: '/templatelist', meta: { role: 'templatelist' },
      name: '模版管理',
      component: body,
      children: [
        {
          path: '/',
          name: '模版', meta: { role: 'templatelist' },
          component: templateTree,
          hidden: true,
          children: [
            {
              path: '/templatelist', meta: { role: 'templatelist' },
              name: '模版列表',
              component: templateList,
              hidden: true
            },
            {
              path: '/templateadd', meta: { role: 'templateadd' },
              name: '添加模版',
              component: templateEdit,
              hidden: true
            },
            {
              path: '/templateedit', meta: { role: 'templateedit' },
              name: '修改模版',
              component: templateEdit,
              hidden: true
            },
            {
              path: '/templaterename', meta: { role: 'templaterename' },
              name: '模版重命名',
              component: templateReName,
              hidden: true
            }, {
              path: '/templatesetting', meta: { role: 'templatesetting' },
              name: '模版设置',
              component: templateSetting,
              hidden: true
            }
          ]
        }
      ]
    },
    {
      path: '/resourcelist', meta: { role: 'resourcelist' },
      name: '资源管理',
      component: body,
      children: [
        {
          path: '/',
          name: '资源', meta: { role: 'resourcelist' },
          component: resourceTree,
          hidden: true,
          children: [
            {
              path: '/resourcelist', meta: { role: 'resourcelist' },
              name: '资源列表',
              component: resourceList,
              hidden: true
            },
            {
              path: '/resourceadd', meta: { role: 'resourceadd' },
              name: '添加资源',
              component: resourceEdit,
              hidden: true
            },
            {
              path: '/resourceedit', meta: { role: 'resourceedit' },
              name: '修改资源',
              component: resourceEdit,
              hidden: true
            },
            {
              path: '/resourcerename', meta: { role: 'resourcerename' },
              name: '资源重命名',
              component: resourceReName,
              hidden: true
            },
          ]
        }
      ]
    },
  ]
},
{
  path: '/',
  name: '设置',
  meta: { role: 'settingConfig' },
  component: header,
  iconCls: 'bbs-setting',
  children: [
    {
      path: '/siteconfig', meta: { role: 'siteconfig' },
      name: '全局设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'siteconfig' },
          component: siteConfig,
          hidden: true
        }]
    },
    {
      path: '/baseconfig', meta: { role: 'baseconfig' },
      name: '站点设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'baseconfig' },
          component: baseConfig,
          hidden: true
        }]
    }, {
      path: '/bbsconfig', meta: { role: 'bbsconfig' },
      name: '论坛设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'bbsconfig' },
          component: bbsConfig,
          hidden: true
        }]
    }, {
      path: '/loginconfig', meta: { role: 'loginconfig' },
      name: '登录设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'loginconfig' },
          component: loginConfig,
          hidden: true
        }]
    }, {
      path: '/pointconfig', meta: { role: 'pointconfig' },
      name: '积分设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'pointconfig' },
          component: pointConfig,
          hidden: true
        }]
    },
    {
      path: '/chargeconfig', meta: { role: 'chargeconfig' },
      name: '打赏设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'chargeconfig' },
          component: chargeConfig,
          hidden: true
        }]
    }, {
      path: '/messageconfig', meta: { role: 'messageconfig' },
      name: '消息提示设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'messageconfig' },
          component: messageConfig,
          hidden: true
        }]
    }, {
      path: '/thirdloginconfig', meta: { role: 'thirdloginconfig' },
      name: '第三方登录设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'thirdloginconfig' },
          component: apiConfig,
          hidden: true
        }]
    }, {
      path: '/ssoconfig', meta: { role: 'ssoconfig' },
      name: '单点登录设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'ssoconfig' },
          component: ssoConfig,
          hidden: true
        }]
    },
    {
      path: '/modelconfig', meta: { role: 'modelconfig' },
      name: '模型配置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'modelconfig' },
          component: modelConfig,
          hidden: true
        }, {
          path: '/addmodelconfig', meta: { role: 'addmodelconfig' },
          name: '添加模型',
          component: modelConfigEdit,
          hidden: true
        }, {
          path: '/editmodelconfig', meta: { role: 'editmodelconfig' },
          name: '修改模型',
          component: modelConfigEdit,
          hidden: true
        }]
    },
    {
      path: '/adconfig', meta: { role: 'adconfig' },
      name: '广告设置',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'adconfig' },
          component: adConfig,
          hidden: true
        }]
    }, {
      path: '/webservicelist', meta: { role: 'webservicelist' },
      name: '接口管理',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'webservicelist' },
          component: webserviceConfig,
          hidden: true
        }, {
          path: '/addwebservice', meta: { role: 'addwebservice' },
          name: '添加接口',
          component: webserviceConfigEdit,
          hidden: true
        }, {
          path: '/editwebservice', meta: { role: 'editwebservice' },
          name: '修改接口',
          component: webserviceConfigEdit,
          hidden: true
        }]
    },
    {
      path: '/webserviceauthlist', meta: { role: 'webserviceauthlist' },
      name: '接口用户管理',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'webserviceauthlist' },
          component: webserviceAuthConfig,
          hidden: true
        }, {
          path: '/addwebserviceauth', meta: { role: 'addwebserviceauth' },
          name: '添加接口用户',
          component: webserviceAuthEdit,
          hidden: true
        }, {
          path: '/editwebserviceauth', meta: { role: 'editwebserviceauth' },
          name: '修改接口用户',
          component: webserviceAuthEdit,
          hidden: true
        }]
    }, {
      path: '/bbslimitlist', meta: { role: 'bbslimitlist' },
      name: '限制Id',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'bbslimitlist' },
          component: bbsLimit,
          hidden: true
        }, {
          path: '/addbbslimit', meta: { role: 'addbbslimit' },
          name: '添加限制用户',
          component: bbsLimitEdit,
          hidden: true
        }, {
          path: '/editbbslimit', meta: { role: 'editbbslimit' },
          name: '修改限制用户',
          component: bbsLimitEdit,
          hidden: true
        }]
    },
    {
      path: '/webapilist', meta: { role: 'webapilist' },
      name: 'API接口管理',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'webapilist' },
          component: apiInfoList,
          hidden: true
        }, {
          path: '/addapiinfo', meta: { role: 'addapiinfo' },
          name: '添加api接口',
          component: apiInfoEdit,
          hidden: true
        }, {
          path: '/editapiinfo', meta: { role: 'editapiinfo' },
          name: '修改api接口',
          component: apiInfoEdit,
          hidden: true
        }]
    },
    {
      path: '/webapiaccountlist', meta: { role: 'webapiaccountlist' },
      name: 'API接口账户管理',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'webapiaccountlist' },
          component: apiAccountList,
          hidden: true
        }, {
          path: '/addapiinfoaccount', meta: { role: 'addapiinfoaccount' },
          name: '添加api接口账户',
          component: apiAccountEdit,
          hidden: true
        }, {
          path: '/editapiinfoaccount', meta: { role: 'editapiinfoaccount' },
          name: '修改api接口账户',
          component: apiAccountEdit,
          hidden: true
        },
        {
          path: '/apiinfoaccountupdate', meta: { role: 'apiinfoaccountupdate' },
          name: '独立密码修改',
          component: apiAccountUpdate,
          hidden: true
        }
      ]
    },
    {
      path: '/webapirecordlist', meta: { role: 'webapirecordlist' },
      name: 'API接口记录',
      component: body,
      children: [
        {
          path: '/', meta: { role: 'webapirecordlist' },
          component: apiRecordList,
          hidden: true
        }]
    }
  ]
},
{
  path: '/401',
  name: '401',
  component: error,
  meta: {
    role: 'index'
  },
  hidden: true
},
{
  path: '*',
  name: '404',
  component: error,
  meta: {
    role: 'index'
  },
  hidden: true
}
]
