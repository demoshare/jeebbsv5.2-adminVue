const roles = [{
  name: '首页',
  role: 'index',
  isHidden: true,
  api: [
    '/api/admin/index',
    '/api/admin/income/indexStatistic'
  ]
},
{
  name: '论坛',
  role: 'bbsparent',
  children: [{
    name: '版块管理',
    role: 'fourmlist',
    api: [
      '/api/admin/forum/listGroupByCategory',
      '/api/admin/category/list',
      '/api/admin/forum/list',
      '/api/admin/forum/get'
    ],
    children: [
      {
        name: '修改',
        role: 'fourmedit',
        api: [
          '/api/admin/forum/batchupdate',
          '/api/admin/category/o_priority_update',
          '/api/admin/forum/update',
        ]
      },
      {
        name: '添加',
        role: 'fourmadd',
        api: [
          '/api/admin/forum/batchupdate'
        ]
      }, {
        name: '删除',
        role: 'fourmdelete',
        api: [
          '/api/admin/category/delete',
          '/api/admin/forum/delete',
        ]
      }
    ]
  }, {
    name: '话题分类管理',
    role: 'topictypelist',
    api: [
      '/api/admin/topicType/tree',
      '/api/admin/topicType/list',
      '/api/admin/topicType/get'
    ],
    children: [{
      name: '修改',
      role: 'topictypeedit',
      api: [
        '/api/admin/topicType/update'
      ]
    },
    {
      name: '添加',
      role: 'topictypeadd',
      api: [
        '/api/admin/topicType/save'
      ]
    }, {
      name: '删除',
      role: 'topictypedelete',
      api: [
        '/api/admin/topicType/delete'
      ]
    }
    ]
  },
  {
    name: '敏感词管理',
    role: 'sensitivitylist',
    api: [
      '/api/admin/sensitivity/list',

    ],
    children: [{
      name: '修改',
      role: 'sensitivityedit',
      api: [
        '/api/admin/sensitivity/batchupdate'
      ]
    },
    {
      name: '添加',
      role: 'sensitivityadd',
      api: [
        '/api/admin/sensitivity/save',
        '/api/admin/sensitivity/batch_save'
      ]
    }, {
      name: '删除',
      role: 'sensitivitydelete',
      api: [
        '/api/admin/sensitivity/delete'
      ]
    }
    ]
  },
  {
    name: '用户举报管理',
    role: 'reportlist',
    api: [
      '/api/admin/report/list',
      '/api/admin/report/get'
    ],
    children: [{
      name: '修改',
      role: 'reportedit',
      api: [
        '/api/admin/report/process'
      ]
    },
    {
      name: '删除',
      role: 'reportdelete',
      api: [
        '/api/admin/report/delete'
      ]
    }
    ]
  }
  ]
},
{
  name: '运营',
  role: 'operation',
  children: [
    {
      name: '友情链接管理',
      role: 'friendlinklist',
      api: [
        '/api/admin/friendLink/list',
        '/api/admin/friendLink/get'
      ],
      children: [{
        name: '添加',
        role: 'friendlinkadd',
        api: [
          '/api/admin/friendLink/save'
        ]
      },
      {
        name: '修改',
        role: 'friendlinkedit',
        api: [
          '/api/admin/friendLink/update',
          '/api/admin/friendLink/priority'
        ]
      },
      {
        name: '删除',
        role: 'friendlinkdelete',
        api: [
          '/api/admin/friendLink/delete'
        ]
      },
      {
        name: '友情链接类别管理',
        role: 'friendlinkctglist',
        api: [
          '/api/admin/friendLinkCtg/list'
        ],
        children: [{
          name: '添加',
          role: 'friendlinkctgadd',
          api: [
            '/api/admin/friendLinkCtg/save'
          ]
        },
        {
          name: '删除',
          role: 'friendlinkctgdelete',
          api: [
            '/api/admin/friendLinkCtg/delete'
          ]
        },
        {
          name: '修改',
          role: 'friendlinkctgedit',
          api: [
            '/api/admin/friendLinkCtg/update'
          ]
        }
        ]
      },
      ]
    },
    {
      name: '广告管理',
      role: 'advertisinglist',
      api: [
        '/api/admin/advertising/list',
        '/api/admin/advertising/get',
        '/api/admin/advertising/check_ad_amount',
        '/api/admin/advertisingSpace/list'
      ],
      children: [{
        name: '添加',
        role: 'advertisingadd',
        api: [
          '/api/admin/advertising/save',
          '/api/admin/advertising/check_ad_amount',
          '/api/admin/user/comparison_username'
        ],
      },
      {
        name: '删除',
        role: 'advertisingdelete',
        api: [
          '/api/admin/advertising/delete'
        ],
      },
      {
        name: '修改',
        role: 'advertisingedit',
        api: [
          '/api/admin/advertising/update',
          '/api/admin/advertising/check_ad_amount',
          '/api/admin/user/comparison_username'
        ],
      },
      {
        name: '广告版位管理',
        role: 'advertisingspacelist',
        api: [
          '/api/admin/advertisingSpace/list',
          '/api/admin/advertisingSpace/get'
        ],
        children: [{
          name: '添加',
          role: 'advertisingspaceadd',
          api: [
            '/api/admin/advertisingSpace/save'
          ]
        }, {
          name: '修改',
          role: 'advertisingspaceedit',
          api: [
            '/api/admin/advertisingSpace/update'
          ]
        }, {
          name: '删除',
          role: 'advertisingspacedelete',
          api: [
            '/api/admin/advertisingSpace/delete'
          ]
        }]
      }
      ]
    },
    {
      name: '礼物中心',
      role: 'giftlist',
      api: [
        '/api/admin/gift/list',
        '/api/admin/gift/get'
      ],
      children: [{
        name: '添加',
        role: 'giftadd',
        api: [
          '/api/admin/gift/save'
        ]
      },
      {
        name: '修改',
        role: 'giftedit',
        api: [
          '/api/admin/gift/update',
          '/api/admin/gift/priority'
        ]
      },
      {
        name: '删除',
        role: 'giftedelete',
        api: [
          '/api/admin/gift/delete'
        ]
      },
      ]
    },
    {
      name: '道具中心',
      role: 'magiclist',
      api: [
        '/api/admin/magic/list',
        '/api/admin/magic/config_get',
        '/api/admin/magic/get'
      ],
      children: [{
        name: '修改',
        role: 'magicedit',
        api: [
          '/api/admin/magic/o_priority'
        ],
      },
      {
        name: '配置',
        role: 'magicconfig',
        api: [
          '/api/admin/magic/config_update',
          '/api/admin/magic/update',
          '/api/admin/magic/o_priority'
        ],
      },
      {
        name: '赠送-选择道具',
        role: 'givemagic',
        api: [
          '/api/admin/magic/give'
        ],
      },
      {
        name: '赠送-选择用户',
        role: 'selectuser',
        api: [
          '/api/admin/group/list',
          '/api/admin/user/list'
        ],
      },
      ]
    },
    {
      name: '提现管理',
      role: 'accountdrawlist',
      api: [
        '/api/admin/accountDraw/list'
      ],
      children: [{
        name: '删除',
        role: 'accountdrawdelete',
        api: [
          '/api/admin/accountDraw/delete'
        ]
      }, {
        name: '提现',
        role: 'accountpayedit',
        api: [
          '/api/admin/accountPay/payByWeiXin',
          '/api/admin/accountPay/list'
        ]
      }]
    },
    {
      name: '转账管理',
      role: 'accountpaylist',
      api: [
        '/api/admin/accountPay/list'
      ],
      children: [{
        name: '删除',
        role: 'accountpaydelete',
        api: [
          '/api/admin/accountPay/delete'
        ],
      }]
    },
    {
      name: '直播管理',
      role: 'livelist',
      api: [
        '/api/admin/live/list',
      ],
      children: [{
        name: '修改',
        role: 'livelist',
        api: [
          '/api/admin/live/check',
          '/api/admin/live/reject',
          '/api/admin/live/start',
          '/api/admin/live/stop'
        ],
      },
      {
        name: '删除',
        role: 'livelist',
        api: [
          '/api/admin/liveHost/delete'
        ],
      },
      {
        name: '配置',
        role: 'liveconfig',
        api: [
          '/api/admin/live/config_get',
          '/api/admin/live/config_update'
        ],
      }
        ,
      {
        name: '主播管理',
        role: 'hostlist',
        api: [
          '/api/admin/liveHost/list',
          '/api/admin/liveApply/list'
        ],
        children: [{
          name: '主播管理',
          role: 'hostlist',
          api: [
            '/api/admin/liveHost/list',
            '/api/admin/liveApply/list'
          ],
          children: [{
            name: '修改',
            role: 'hostedit',
            api: [
              '/api/admin/liveApply/check',
              '/api/admin/liveApply/reject',
              '/api/admin/liveHost/priority'
            ],
          },
          {
            name: '删除',
            role: 'hostdelete',
            api: [
              '/api/admin/liveHost/delete',
              '/api/admin/liveApply/delete'
            ],
          },
          ]
        }]
      },
      ]
    },

    {
      name: '收费统计',
      role: 'chargelist',
      api: [
        '/api/admin/order/charge_list'
      ]
    },
    {
      name: '用户账户统计',
      role: 'accountlist',
      api: [
        '/api/admin/order/account_list'
      ]
    },
    {
      name: '订单流水',
      role: 'userorderlist',
      api: [
        '/api/admin/order/user_order_list'
      ]
    },
    {
      name: '收益统计',
      role: 'incomeStatisticlist',
      api: [
        '/api/admin/data/incomeStatistic_list'
      ],
      children: [{
        name: '导出',
        role: 'incomeStatisticlistexport',
      }]
    },
    {
      name: '数据中心',
      role: 'forumstatisticlist',
      api: [
        '/api/admin/data/forumstatistic_list'
      ],
      children: [{
        name: '导出',
        role: 'forumstatisticlistexport',
      }]
    }
  ]
},
{
  name: '用户',
  role: 'user',
  children: [{
    name: '用户组管理',
    role: 'usergroup',
    api: [
      '/api/admin/group/list',
      '/api/admin/group/get'
    ],
    children: [{
      name: '添加',
      role: 'usergroupadd',
      api: [
        '/api/admin/upload/o_upload',
        '/api/admin/group/save'
      ],
    },
    {
      name: '修改',
      role: 'usergroupedit',
      api: [
        '/api/admin/upload/o_upload',
        '/api/admin/group/update'
      ],
    },
    {
      name: '删除',
      role: 'usergroupdelete',
      api: [
        '/api/admin/group/delete'
      ],
    },

    ]
  }, {
    name: '用户管理',
    role: 'userlist',
    api: [
      '/api/admin/user/list',
      '/api/admin/user/get'
    ],
    children: [{
      name: '添加',
      role: 'useradd',
      api: [
        '/api/admin/user/save'
      ],
    },
    {
      name: '修改',
      role: 'useredit',
      api: ['/api/admin/user/update'],
    },
    {
      name: '删除',
      role: 'userdelete',
      api: [
        '/api/admin/user/delete'
      ],
    },
    ]
  }, {
    name: '管理员管理',
    role: 'adminlist',
    api: [
      '/api/admin/admin/list',
      '/api/admin/user/get'
    ],
    children: [{
      name: '添加',
      role: 'adminadd',
      api: [
        '/api/admin/admin/save'
      ],
    },
    {
      name: '修改',
      role: 'adminedit',
      api: [
        '/api/admin/admin/update'
      ],
    },
    {
      name: '删除',
      role: 'admindelete',
      api: [
        '/api/admin/admin/delete'
      ],
    },
    ]
  },
  {
    name: '官网团队账户',
    role: 'officiallist',
    api: [
      '/api/admin/user/official_list',
      '/api/admin/group/list',
      '/api/admin/user/get'
    ],
    children: [{
      name: '添加',
      role: 'officialadd',
      api: [
        '/api/admin/user/save'
      ],
    },
    {
      name: '修改',
      role: 'officialedit',
      api: [
        '/api/admin/user/update'
      ],
    },
    {
      name: '删除',
      role: 'officialdelete',
      api: [
        '/api/admin/user/delete'
      ],
    },
    ]
  }, {
    name: '账户绑定',
    role: 'useraccountlist',
    api: [
      '/api/admin/account/list'
    ],
    children: [{
      name: '删除',
      role: 'useraccountdelete',
      api: [
        '/api/admin/account/delete'
      ],
    },]
  },
  {
    name: '角色管理',
    role: 'rolelist',
    api: [
      '/api/admin/role/list',
      '/api/admin/role/get'
    ],
    children: [{
      name: '添加',
      role: 'roleadd',
      api: [
        '/api/admin/role/save'
      ],
    },
    {
      name: '修改',
      role: 'roleedit',
      api: [
        '/api/admin/role/update'
      ],
    },
    {
      name: '删除',
      role: 'roledelete',
      api: [
        '/api/admin/role/delete'
      ],
    },
    ]
  },
  {
    name: '系统消息',
    role: 'systemmessagelist',
    api: [
      '/api/admin/message/sys_list'
    ],
    children: [{
      name: '添加',
      role: 'systemmessageadd',
      api: [
        '/api/admin/message/sendSys',
        '/api/admin/group/list',
      ],
    },
    {
      name: '删除',
      role: 'systemmessagedelete',
      api: [
        '/api/admin/message/delete'
      ],
    },
    ]
  },
  ]
},
{
  name: '界面',
  role: 'interface',
  children: [{
    name: '资源',
    role: 'resourcelist',
    api: [
      '/api/admin/resource/list',
      '/api/admin/resource/tree',
      '/api/admin/resource/get'
    ],
    children: [{
      name: '添加',
      role: 'resourceadd',
      api: [
        '/api/admin/resource/dir_save',
        '/api/admin/resource/save'
      ]
    },
    {
      name: '修改',
      role: 'resourceedit',
      api: [
        '/api/admin/resource/dir_save',
        '/api/admin/template/update'
      ]
    }, {
      name: '删除',
      role: 'resourcedelete',
      api: [
        '/api/admin/resource/delete'
      ]
    },
    {
      name: '重命名',
      role: 'resourcerename',
      api: [
        '/api/admin/resource/rename'
      ]
    }, {
      name: '上传',
      role: 'resourcereupload',
      api: [
        '/api/admin/resource/upload'
      ]
    }
    ]
  },
  {
    name: '模版',
    role: 'templatelist',
    api: [
      '/api/admin/template/list',
      '/api/admin/template/tree',
      '/api/admin/template/get'
    ],
    children: [{
      name: '添加',
      role: 'templateadd',
      api: [
        '/api/admin/template/dir_save',
        '/api/admin/template/save'
      ]
    },
    {
      name: '修改',
      role: 'templateedit',
      api: [
        '/api/admin/template/dir_save',
        '/api/admin/template/update'
      ]
    }, {
      name: '删除',
      role: 'templatedelete',
      api: [
        '/api/admin/template/delete'
      ]
    },
    {
      name: '重命名',
      role: 'templaterename',
      api: [
        '/api/admin/template/rename'
      ]
    }, {
      name: '上传',
      role: 'templateupload',
      api: [
        '/api/admin/template/upload'
      ]
    }, {
      name: '模版设置',
      role: 'templatesetting',
      api: [
        '/api/admin/resource/upload',
        '/api/admin/template/getSolutions',
        '/api/admin/template/solutionupdate'
      ]
    }
    ]
  }
  ]
},
{
  name: '设置',
  role: 'settingConfig',
  children: [
    {
      name: '全局设置',
      role: 'siteconfig',
      api: [
        '/api/admin/site_config/system_get',
      ], children: [
        {
          name: '修改',
          role: 'siteconfigedit',
          api: [
            '/api/admin/site_config/system_update'
          ]
        }
      ]
    },
    {
      name: '站点设置',
      role: 'baseconfig',
      api: [
        '/api/admin/site_config/base_get'
      ], children: [
        {
          name: '修改',
          role: 'baseconfigedit',
          api: [
            '/api/admin/site_config/base_update',
          ]
        }
      ]
    },
    {
      name: '论坛设置',
      role: 'bbsconfig',
      api: [
        '/api/admin/bbs_config/get',
        '/api/admin/group/list'
      ], children: [
        {
          name: '修改',
          role: 'bbsconfigedit',
          api: [
            '/api/admin/bbs_config/update',
          ]
        }
      ]
    },
    {
      name: '登录设置',
      role: 'loginconfig',
      api: [
        '/api/admin/bbs_config/login_get'
      ], children: [
        {
          name: '修改',
          role: 'loginconfigedit',
          api: [
            '/api/admin/bbs_config/login_update',
          ]
        }
      ]
    },
    {
      name: '积分设置',
      role: 'pointconfig',
      api: [
        '/api/admin/bbs_config/creditExchange_get'
      ], children: [
        {
          name: '修改',
          role: 'pointconfigedit',
          api: [
            '/api/admin/bbs_config/creditExchange_update',
          ]
        }
      ]
    },
    {
      name: '打赏设置',
      role: 'chargeconfig',
      api: [
        '/api/admin/bbs_config/charge_get'
      ], children: [
        {
          name: '修改',
          role: 'chargeconfigedit',
          api: [
            '/api/admin/bbs_config/charge_update',
          ]
        }
      ]
    },
    {
      name: '消息提示设置',
      role: 'messageconfig',
      api: [
        '/api/admin/bbs_config/message_get'
      ], children: [
        {
          name: '修改',
          role: 'messageconfigedit',
          api: [
            '/api/admin/bbs_config/message_update',
          ]
        }
      ]
    },
    {
      name: '第三方登录设置',
      role: 'thirdloginconfig',
      api: [
        '/api/admin/bbs_config/api_get'
      ], children: [
        {
          name: '修改',
          role: 'thirdloginconfigedit',
          api: [
            '/api/admin/bbs_config/api_update',
          ]
        }
      ]
    },
    {
      name: '单点登录设置',
      role: 'ssoconfig',
      api: [
        '/api/admin/bbs_config/sso_get'
      ], children: [
        {
          name: '修改',
          role: 'thirdloginconfigedit',
          api: [
            '/api/admin/bbs_config/sso_update'
          ]
        }
      ]
    },
    {
      name: '模型配置',
      role: 'modelconfig',
      api: [
        '/api/admin/bbs_config/item_list',
        '/api/admin/bbs_config/item_get'
      ], children: [
        {
          name: '添加',
          role: 'addmodelconfig',
          api: [

            '/api/admin/bbs_config/item_save',
            '/api/admin/bbs_config/item_priority'
          ]
        },
        {
          name: '修改',
          role: 'editmodelconfig',
          api: [
            '/api/admin/bbs_config/item_update',
            '/api/admin/bbs_config/item_priority'
          ]
        },
        {
          name: '删除',
          role: 'modelconfigdelete',
          api: [
            '/api/admin/bbs_config/item_delete'
          ]
        }
      ]
    }, {
      name: '广告设置',
      role: 'adconfig',
      api: [
        '/api/admin/bbs_config/ad_get'
      ], children: [
        {
          name: '修改',
          role: 'adconfigedit',
          api: [
            '/api/admin/bbs_config/ad_update'
          ]
        }
      ]
    },
    {
      name: '接口管理',
      role: 'webservicelist',
      api: [
        '/api/admin/webservice/list',
        '/api/admin/webservice/get'
      ], children: [
        {
          name: '添加',
          role: 'addwebservice',
          api: [

            '/api/admin/webservice/save'
          ]
        },
        {
          name: '修改',
          role: 'editwebservice',
          api: [
            '/api/admin/webservice/update'
          ]
        },
        {
          name: '删除',
          role: 'webservicedelete',
          api: [
            '/api/admin/webservice/delete'
          ]
        }
      ]
    },
    {
      name: '接口用户管理',
      role: 'webserviceauthlist',
      api: [
        '/api/admin/webserviceAuth/list',
        '/api/admin/webserviceAuth/get'
      ], children: [
        {
          name: '添加',
          role: 'addwebserviceauth',
          api: [
            '/api/admin/webserviceAuth/save'
          ]
        },
        {
          name: '修改',
          role: 'editwebserviceauth',
          api: [
            '/api/admin/webserviceAuth/update'
          ]
        },
        {
          name: '删除',
          role: 'webserviceauthedit',
          api: [
            '/api/admin/webserviceAuth/delete'
          ]
        }
      ]
    },
    {
      name: '限制Id',
      role: 'bbslimitlist',
      api: [
        '/api/admin/bbslimit/list',
        '/api/admin/bbslimit/get'
      ], children: [
        {
          name: '添加',
          role: 'addbbslimit',
          api: [
            '/api/admin/bbslimit/save'
          ]
        },
        {
          name: '修改',
          role: 'editbbslimit',
          api: [
            '/api/admin/bbslimit/update'
          ]
        },
        {
          name: '删除',
          role: 'bbslimitdelete',
          api: [
            '/api/admin/bbslimit/delete'
          ]
        }
      ]
    },
    {
      name: 'API接口管理',
      role: 'webapilist',
      api: [
        '/api/admin/apiInfo/list',
        '/api/admin/apiInfo/get'
      ], children: [
        {
          name: '添加',
          role: 'addapiinfo',
          api: [
            '/api/admin/apiInfo/save'
          ]
        },
        {
          name: '修改',
          role: 'editapiinfo',
          api: [
            '/api/admin/apiInfo/update'
          ]
        },
        {
          name: '删除',
          role: 'apiinfodelete',
          api: [
            '/api/admin/apiInfo/delete'
          ]
        }
      ]
    },
    {
      name: 'API接口账户管理',
      role: 'webapiaccountlist',
      api: [
        '/api/admin/apiAccount/list',
        '/api/admin/apiAccount/get'
      ], children: [
        {
          name: '添加',
          role: 'addapiinfoaccount',
          api: [
            '/api/admin/apiAccount/save',
            '/api/admin/config_api_pwd/validate'
          ]
        },
        {
          name: '独立密码修改',
          role: 'apiinfoaccountupdate',
          api: [
            '/api/admin/config_api_pwd/update'
          ]
        }
      ]
    },
    {
      name: 'API接口记录',
      role: 'webapirecordlist',
      api: [
        '/api/admin/apiRecord/list'
      ], children: [
        {
          name: '列表',
          role: '/webapirecordlist',
          api: [
            '/api/admin/apiRecord/list'
          ]
        },
        {
          name: '删除',
          role: 'webapirecordlistdelete',
          api: [
            '/api/admin/apiRecord/delete'
          ]
        }
      ]
    },

  ]
}
]
export default roles
