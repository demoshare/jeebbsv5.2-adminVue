var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  Base_URL:'"http://192.168.0.173:8007/jeebbs5"'
})
